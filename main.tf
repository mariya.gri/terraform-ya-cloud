terraform {
  required_version = "= 1.3.4"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "= 0.73"
    }
  }
}

provider "yandex" {
  token     = "y0_AgAAAAAcOMtaAATuwQAAAADTZSgpUO3-a6i4Qw-iraaCrptmGDZ8Seg"
  cloud_id  = "fhm9i5nsj0e05kvges8g"
  folder_id = "b1ghvjhkea5mkf6cggpp"
  zone      = "ru-central1-a"
}


resource "yandex_compute_instance" "test" {
  name = "test"


  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_1.id
    nat       = true
  }


  metadata = {
    user-data = "${file("/opt/terraform/yandex/meta.txt")}"
      ssh-keys = "ubuntu:${file("~/.ssh/yandex-terraform1.pub")}"
  }


  provisioner "file" {
    source      = "scripts/create.sh"
    destination = "/tmp/create.sh"
    connection {
      type        = "ssh"
      user        = "viva"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = yandex_compute_instance.test.network_interface[0].nat_ip_address
    }
  }

  # Могут быть ошибки чтения файла, исправить sed -i -e 's/\r$//' create.sh
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/create.sh",
      "/tmp/create.sh",
    ]
    connection {
      type        = "ssh"
      user        = "viva"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = yandex_compute_instance.test.network_interface[0].nat_ip_address
    }
  }  

  
  lifecycle {
    create_before_destroy = true
  }

}

resource "yandex_vpc_network" "network_1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet_1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_1.id
  v4_cidr_blocks = ["192.168.15.0/24"]
  depends_on = [
  yandex_vpc_network.network_1
  ]
}


# Выведем IP адрес сервера
output "my_web_site_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = yandex_compute_instance.test.network_interface[0].ip_address
}

output "external_ip_address" {
  value = yandex_compute_instance.test.network_interface[0].nat_ip_address
}
